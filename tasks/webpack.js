'use strict';
const	$ = require('gulp-load-plugins')(),
		webpackstream = require('webpack-stream'),
		webpack = require('webpack'), 
		named = require('vinyl-named'),
		path  = require('path'),
		gulp = require('gulp');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

process.noDeprecation = true
module.exports = function(options) {
	return function() {
		console.log(options.src);
		let optionsWebpack = {
		  watch:   true,
			devtool: isDevelopment ? 'cheap-module-inline-source-map' : null,
			
		  module:  {
		    rules: [
					{
						test: /\.(js|jsx)$/,
						loader: "babel-loader",
						query: {
							presets: ['es2015', 'react']
						}
					},
					{
						test: /\.css$/,						
						use: [ 'style-loader', 'css-loader' ]
					},
					{ test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' }
				]
			},
			resolve: {
				extensions: [" ",'.js', '.jsx', 'css']    
			},
		  plugins: [
		    new webpack.ProvidePlugin({
		      $:      "jquery",
		      jQuery: "jquery",
		          _: "loadash",
		      "window.jQuery": "jquery"
		    })
		  ]
		};

		return gulp.src(options.src)
		    .pipe($.plumber({
		      errorHandler: $.notify.onError(err => ({
		        title:   ' ',
		        message: err.message
		      }))
		    }))
		    .pipe(named())
		    .pipe(webpackstream(optionsWebpack, webpack))
			.on('error', function(error) {
				plugins.util.log(plugins.util.colors.red(error.message));
				this.emit('end');
			})
			.pipe(gulp.dest(options.dst))

		}
};